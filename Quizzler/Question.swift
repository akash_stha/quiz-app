//
//  Question.swift
//  Quizzler
//
//  Created by Newarpunk on 12/23/19.
//  Copyright © 2019 London App Brewery. All rights reserved.
//

import Foundation

class Question {
    var questionText : String
    var answer : Bool
    
    init(text : String , correctAnswer : Bool) {
        questionText = text
        answer = correctAnswer
    }
}
